#include <stdio.h>
#include <stdlib.h>

typedef struct BinaryTreeNode
{
	int data;					//информационное поле, играющее роль ключа
	BinaryTreeNode* left;		//указатель на узел, являющийся корнем левого поддерева
	BinaryTreeNode* right;		//указатель на узел, являющийся корнем правого поддерева
} BinaryTreeNode;

BinaryTreeNode* addNode(BinaryTreeNode* root, int value);					//добавление узла - рекурсивный стиль
BinaryTreeNode* addNode_no_recursion(BinaryTreeNode* root, int value);		//добавление узла без рекурсии
BinaryTreeNode* searchNode(BinaryTreeNode* root, int value);				//поиск узла по ключу - рекурсивный стиль
BinaryTreeNode* searchNode_no_recursion(BinaryTreeNode* root, int value);	//поиск узла по ключу без рекурсии
BinaryTreeNode* removeNode(BinaryTreeNode* root, int value);				//удаление узла по ключу
BinaryTreeNode* getMinNode(BinaryTreeNode* root);		//поиск узла с минимальным значением ключа
BinaryTreeNode* getMaxNode(BinaryTreeNode* root);		//поиск узла с максимальным значением ключа
void printSortAscending(BinaryTreeNode* root);			//обход дерева, выдающий сортированный по возрастанию список		
void printSortDescending(BinaryTreeNode* root);			//обход дерева, выдающий сортированный по убыванию список
void destroyTree(BinaryTreeNode* root);			//освобождение всей динамически распределённой для хранения узлов дерева памяти

int main()
{
	BinaryTreeNode* root = NULL;	//не забываем инициализировать!

	//формируем дерево
	root = addNode(root, 5);		//для корня дерева - принципиально важно брать возвращаемое значение!
	root = addNode(root, -3);		//для остальных узлов уже не обязательно не обновлять root
	root = addNode(root, 10);
	root = addNode(root, 8);
	root = addNode(root, 6);
	root = addNode(root, 3);
	root = addNode(root, -7);
	root = addNode(root, -5);
	root = addNode(root, 0);
	root = addNode(root, 20);

	//аналогичный код, но использующий нерекурсивную реализацию добавления элементов
	/*root = addNode_no_recursion(root, 5);
	root = addNode_no_recursion(root, -3);
	root = addNode_no_recursion(root, 10);
	root = addNode_no_recursion(root, 8);
	root = addNode_no_recursion(root, 6);
	root = addNode_no_recursion(root, 3);
	root = addNode_no_recursion(root, -7);
	root = addNode_no_recursion(root, -5);
	root = addNode_no_recursion(root, 0);
	root = addNode_no_recursion(root, 20);*/

	//проверяем работу функций поиска минимума и максимума
	printf("min = %d\n", getMinNode(root)->data);
	printf("max = %d\n", getMaxNode(root)->data);

	//проверяем работу функций обхода по дереву
	printSortAscending(root);
	printf("\n");
	printSortDescending(root);
	printf("\n");

	//проверяем работу функции удаления узла
	//демонстрация на самом сложном случае:
	//5 - это корень всего дерева, имеющий обоих потомков
	root = removeNode(root, 5);

	//отслеживаем изменения после удаления узла
	printSortAscending(root);
	printf("\n");
	printSortDescending(root);
	printf("\n");
	
	//не забываем про необходимость очистки памяти
	destroyTree(root);

	getchar();
    return 0;
}

BinaryTreeNode* addNode(BinaryTreeNode* root, int value)
{
	/*
	Обратите внимание!
	Во всех функциях мы передаём указатель на BinaryTreeNode,
	но при этом создаётся копия этого указателя,
	с которой работает функция.
	Если нам нужно поменять сам указатель - нужно либо использовать
	конструкцию типа "указатель на указатель", либо пользоваться
	возвращаемым значением.
	*/

	//проверяем простейший случай - дерево (поддерево) пусто
	if (!root)
	{
		//выделяем память под новый узел
		root = (BinaryTreeNode*)malloc(sizeof(BinaryTreeNode));
		//инициализируем поля созданного узла
		root->data = value;
		root->left = NULL;
		root->right = NULL;
		//сразу выходим из функции
		return root;
	}

	//добавляемый элемент должен попасть в правое поддерево
	if (value > root->data)
		root->right = addNode(root->right, value);

	//добавляемый элемент должен попасть в левое поддерево
	if (value < root->data)
		root->left = addNode(root->left, value);

	//корень не изменился, но всё равно его нужно вернуть
	return root;
}

BinaryTreeNode* addNode_no_recursion(BinaryTreeNode* root, int value)
{
	BinaryTreeNode* current = root;
	
	//подготовим узел-лист для вставки,
	//а уже потом прицепим его к существующему дереву
	BinaryTreeNode* newNode;
	newNode = (BinaryTreeNode*)malloc(sizeof(BinaryTreeNode));
	newNode->data = value;
	newNode->left = NULL;
	newNode->right = NULL;

	//условие продолжения цикла следует понимать как
	//обход тела цикла, если current изначально NULL
	while (current)
	{
		//рассматриваем лишь узлы с уникальными ключами
		if (value == current->data)
		{
			//поэтому дубль не создаём,
			//новый узел тут же удаляем
			free(newNode);
			return root;
		}

		//новый узел должен попасть в левое поддерево
		if (value < current->data)
			//и в левом поддереве есть элементы
			if (current->left)
			{
				current = current->left;
				continue;
			}
			//иначе уже понятно, куда прицепить новый лист
			else
			{
				current->left = newNode;
				return root;
			}
		
		//обработка правого поддерева - аналогично
		if (value > current->data)
			if (current->right)
			{
				current = current->right;
				continue;
			}
			else
			{
				current->right = newNode;
				return root;
			}
	}
	return newNode;	//срабатывае только когда current изначально равен NULL
}

BinaryTreeNode* searchNode(BinaryTreeNode* root, int value)
{
	//если дерево пусто - искать в нём просто нечего
	if (!root)
		return NULL;

	//если нашли, что искали - вернули адрес узла дерева
	if (root->data == value)
		return root;

	//иначе продолжаем рекурсинвый поиск
	if (value > root->data)
		//в правом поддереве
		return searchNode(root->right, value);
	else
		//или левом поддереве
		return searchNode(root->left, value);
}

BinaryTreeNode* searchNode_no_recursion(BinaryTreeNode* root, int value)
{
	//root - это всего лишь копия адреса корня дерева,
	//его можно менять, не опасаясь испортить оригинал
	
	while (root)
	{
		//если нашли, что искали
		if (value == root->data)
			return root;

		//иначе продолжаем двигаться вглубь дерева
		if (value < root->data)
			root = root->left;
		else
			root = root->right;
	}
	//если дошли до конца, но не нашли узла
	return NULL;
}

BinaryTreeNode* removeNode(BinaryTreeNode* root, int value)
{
	BinaryTreeNode* temp;
	
	//если дерево пусто, из него нечего удалять
	if (!root)
		return NULL;

	//если узел принадлежит правому поддереву
	if (value > root->data)
		//рекурсивно вызываем функцию
		//обратите внимание на обновление root->right
		root->right = removeNode(root->right, value);
	
	//аналогично с левым поддеревом
	if (value < root->data)
		root->left = removeNode(root->left, value);

	//если текущий узел - тот, который нужно удалить
	if (root->data == value)
	{
		//если узел не имеет потомков (лист)
		if (!root->left && !root->right)
		{
			free(root);
			return NULL;
		}

		//если у узла есть только левый потомок
		if (root->left && !root->right)
		{
			temp = root->left;
			free(root);
			return temp;
		}

		//если у узла есть только правый потомок
		if (!root->left && root->right)
		{
			temp = root->right;
			free(root);
			return temp;
		}

		//если у узла есть оба потомка
		if (root->left && root->right)
		{
			//внимательно читаем названия переменных
			int max_value_in_left_subtree = getMaxNode(root->left)->data;
			int min_value_in_right_subtree = getMinNode(root->right)->data;

			//находим из них то, которое ближе к значению ключа рассматриваемого узла
			if ((root->data - max_value_in_left_subtree) < (min_value_in_right_subtree - root->data))
			{
				//не удаляем рассматриваемый элемент - вместо этого
				//удаляем элемент из поддерева, наиболее близкий к нему
				removeNode(root->left, max_value_in_left_subtree);
				//сохраняем ссылочную структуру дерева,
				//но обновляем содержимое информационных полей
				//(в нашем случае - только data)
				root->data = max_value_in_left_subtree;
			}
			else
			{
				//аналогично для правого поддерева
				removeNode(root->right, min_value_in_right_subtree);
				root->data = min_value_in_right_subtree;
			}
		}
	}
		
	return root;
}

BinaryTreeNode* getMinNode(BinaryTreeNode* root)
{
	//если дерево пусто, в нём нечего искать
	if (!root)
		return NULL;

	//двигаемся влево до тех пор,
	//пока у очередного узла есть левый потомок
	while (root->left)
		root = root->left;

	//возвращаем узел, у которого левый потомок стал NULL
	return root;
}

BinaryTreeNode* getMaxNode(BinaryTreeNode* root)
{
	//если дерево пусто, в нём нечего искать
	if (!root)
		return NULL;

	//двигаемся вправо до тех пор,
	//пока у очередного узла есть правый потомок
	while (root->right)
		root = root->right;

	//возвращаем узел, у которого правый потомок стал NULL
	return root;
}

void printSortAscending(BinaryTreeNode* root)
{
	//точка выхода из рекурсии
	// + защита от попытки обойти пустое дерево
	if (!root)
		return;

	//следующий if не обязателен,
	//но позволяет избежать последнего рекурсивного вызова
	//if (root->left)
		printSortAscending(root->left);		//обхдим левое поддерево

	printf("%5d", root->data);				//печатаем содержимое узла

	//if (root->right)
		printSortAscending(root->right);	//обхдим правое поддерево
}

void printSortDescending(BinaryTreeNode* root)
{
	/*
	аналогично предыдущей функции,
	меняется только порядок обхода поддеревьев
	*/

	if (!root)
		return;

	//if (root->right)
		printSortDescending(root->right);

	printf("%5d", root->data);

	//if (root->left)
		printSortDescending(root->left);
}

void destroyTree(BinaryTreeNode* root)
{
	//в основе - тоже обход дерева,
	//только центральный узел просматривается
	//после обхода поддеревьев

	if (root)
	{
		//if (root->left)
			destroyTree(root->left);
		//if (root->right)
			destroyTree(root->right);
		free(root);
	}
}